<?php
/**
 * THIS WILL BE DELETED AND MOVED ONTO LINCUS VERSION
 */

class AlexaClass{
    //THIS DOES NOT NEED COPYING OVER
    /**
     * API Constants
     *
     */
    private $db_conn;


    /**
     * Class Variables
     *
     */
    private $userId;
    private $deviceId = null;
    private $lastError = null;
    private $LincusAPI;
    private $moduleID = 19;

    /**
     * Constructor
     * @param $vars
     * @param null $db_conn
     * @param $userID
     * @param null $deviceId
     * @param $LincusAPI
     */
    public function __construct($db_conn = null, $userID, $deviceId = null, $LincusAPI = null) {

        $this->db_conn = $db_conn;
        $this->userId = $userID;
        $this->deviceId = $deviceId; //TODO Get the last primary key of access_token...
        $this->LincusAPI = $LincusAPI;
    }
    //END OF NO COPY

    public function insertDetail($intentName, $slotData = null, $address = null){
        $commandId = null;
        try {
            $this->db_conn->beginTransaction();

            //Get the command id
            $query = $this->db_conn->prepare("SELECT command_id FROM alexa_commands WHERE command_intent=:intentName");
            $query->bindValue(":intentName", $intentName);
            $query->execute();
            $commandResult = $query->fetch(PDO::FETCH_ASSOC);

            if (!is_null($commandResult)){
                foreach($commandResult as $result){
                    $commandId = $result;
                }
            }

            //Save to the alexa details table
            if (!is_null($commandId)) {
                $query2 = $this->db_conn->prepare("INSERT INTO alexa_details (command_id, date, time, userID, editID, alexa_slot, address_string)"
                    . " VALUES(:commandid, CURDATE(), CURTIME(), :userid, :editid, :slotData, :address)");
                $query2->bindValue(":commandid", $commandId);
                //$query2->bindValue(":date", date("d M Y"));
                //$query2->bindValue(":time", date("H:i:s"));
                $query2->bindValue(":userid", $this->userId);
                $query2->bindValue(":editid", $this->userId);
                $query2->bindValue(":slotData", $slotData);
                $query2->bindValue(":address", $address);

                $query2->execute();
            }

            $this->db_conn->commit();

        }catch (PDOException $e){
            $this->db_conn->rollback();
            ErrorHandler::buildErrorNoResponse("An error happened when inserting an detail to the DB: " . $e->getMessage());
        }
    }
}