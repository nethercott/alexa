# Amazon Alexa PHP Library

This is a library for Lincus to handle and integrate with an amazon alexa device. This project handles all data to and 
from an Amazon Alexa enabled device. It should be a fully documented and commented Library for anyone to use and integrate 
with an Amazon Alexa.
## Getting Started

To get started with library clone or add as a module to your existing codebase. Then see the file example.php for an example 
to read data from the initial payload sent from the Alexa enabled device and sending a response back.
### Prerequisites

Account Linking must be setup on the Alexa Skill with a separate oAuth2.0 server setup. This library is an 
Endpoint for the Amazon Alexa written in PHP not an oAuth server. 

## Using the Library
Use the HandleRequest class to verify the message sent from Alexa and to ensure that the user has successfully linked their 
account. This is all done in the constructor of the class so you can easily verify by just doing:
 
```php
$payload = json_decode(file_get_contents("php://input"), true);

$session = $payload["session"];
$applicationId = $session["application"]["applicationId"];

$accessToken = $session["user"]["accessToken"];

$handleRequest = new HandleRequest($applicationId, $accessToken, $db_conn);
```
One more step is needed to initialise and verify the connection. You need to add the skill ID value for alexa to the array 
$supported_application_ids in HandleRequest. Add all skills that are to be handled to this array. The Library is designed to handle
many skills and built for rigidity. 

Once the request has been verified you can process the Alexa data and prepare a response to send to the user. 
An example of this can be found in example.php.

A simple hello world setup could be:
```php
//build the response message
$response = new HandleResponse();
$intentHandler = new HandleIntent($intent);

//Build a card for alexa app or Echo Show Display
$card = new CardResponse();
$card->createSimple("Lincus", "Hello World");

$response->newAskResponse("Hello World");

$responseToSend["version"] = "1.0";
$responseToSend["card"] = $card->getCard();
$responseToSend["response"] = $response->getResponse();
```

It is possible to create session variables and this will be saved in the JSON response and 
passed back to the Alexa device. To do this:

```php
$session = new CreateSession();
$session->addToSession("Session Name", "Session Data");

$responseToSend["sessionAttributes"] = $session->getSession(); //Add All session attributes to the message
```
## Authors

**Matthew Nethercott** - *Initial work*