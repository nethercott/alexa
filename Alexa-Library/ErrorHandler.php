<?php
/**
 * <h1>ErrorHandler</h1>
 * <br />
 * Handle all error messages that could occur with during the processing.
 * Return the error message to the user so they can act accordingly.
 * @author Matthew Nethercott
 * @version 1.0
 * @since 11 June 2018
 */

class ErrorHandler {

    /**
     * <h2>buildError</h2>
     * <br />
     * Build an error message and return this to the user. If the error is non-technical (something the user can fix)
     * return the message to the user. (use this one if they can fix it, like logging in to Lincus).
     * @param $errorMessage String error to display to the user
     * @return string JSON string to return
     */
    public static function buildError($errorMessage){
        $error = new \ArrayObject();

        $response = new ResponseHandler();
        $response->newTellResponse($errorMessage);

        $error["response"] = $response->getResponse();

        file_put_contents("log.txt", "DATE:" . date("Y-m-d H:i:s") . " ERROR: " . $errorMessage . "\n", FILE_APPEND);
        //LincusErrorHandling::saveError($errorMessage);

        return json_encode($error);
    }

    /**
     * <h2>buildErrorNoResponse</h2>
     * <br />
     * Use this error message if something technical has happened. Like DB not accessible, this will log in Lincus and
     * return a generic error message to the user telling them to report the problem.
     * @param $errorMessage String what to log on Lincus
     * @return string JSON string of generic error to report.
     */
    public static function buildErrorNoResponse($errorMessage){
        $error = new \ArrayObject();

        $response = new ResponseHandler();
        $response->newTellResponse("An error has occurred. Please report to the skill administrator");

        $error["response"] = $response->getResponse();

        file_put_contents("log.txt", "DATE:" . date("Y-m-d H:i:s") . " ERROR: " . $errorMessage . "\n", FILE_APPEND);

        //LincusErrorHandling::saveError($errorMessage);

        return json_encode($error);
    }

    /**
     * <h2>buildError</h2>
     * <br />
     * Build an error message that has occurred during the Login process. Notify the user that they need to link their
     * Lincus Account and display the correct card to them.
     * @param $errorMessage String error to display to the user
     * @return string JSON string to return
     */
    public static function buildLoginError($errorMessage){
        $error = new \ArrayObject();

        $response = new ResponseHandler();
        $response->newLinkResponse($errorMessage, "Lincus Alerts");

        $error["response"] = $response->getResponse();

        file_put_contents("log.txt", "DATE:" . date("Y-m-d H:i:s") . " ERROR: " . $errorMessage . "\n", FILE_APPEND);
        //LincusErrorHandling::saveError($errorMessage);

        return json_encode($error);
    }
}