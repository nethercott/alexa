<?php

$db_conn = null; //<-- This variable would be defined in Lincus. Would not need instantiating if used for Lincus
$payload = json_decode(file_get_contents("php://input"), true);

//INTIALISE DATA SENT FROM ALEXA
//get session data and verification data
$session = $payload["session"];
$applicationId = $session["application"]["applicationId"];
$user = $session["user"];

//Get the request and the intent sent from Alexa
$request = $payload["request"];
$accessToken = $user["accessToken"];
if ($request["type"] == "IntentRequest")    $intent = $request["intent"];
//END OF INITIALISATION

//setup the handler of the this data and verify the request
$handler = new RequestHandler($applicationId, $accessToken, $db_conn);

//build the response message
$response = new ResponseHandler();
$intentHandler = new IntentHandler($intent);

//Build a card for alexa app or Echo Show Display
$card = new CardResponse();
$card->createSimple("Lincus", "Hello World");

$helpSlot = $intentHandler->getIntentSlotValue("Help");
$response->newAskResponse("Hello World");

$responseToSend["version"] = "1.0";
$responseToSend["card"] = $card->getCard();
$responseToSend["response"] = $response->getResponse();

die (json_encode($responseToSend));