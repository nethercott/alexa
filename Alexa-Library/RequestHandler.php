<?php
//Is account linking for this endpoint enabled? By default YES
define ("ACCOUNT_LINKING", TRUE);
/**
 * <h1>RequestHandler</h1>
 * <br />
 * Handle the request sent from the alexa enabled device. This class verifies it is from a supported skill for use with
 * Lincus.
 * @author Matthew Nethercott
 * @version 1.0.0
 * @since 8th June 2018
 */

class RequestHandler {
    /**
     * Add all allowed skills to this array.
     * This library can be used handle multiple alexa skills
     */
    private $supported_application_ids = array(
        'applicationId' => 'amzn1.ask.skill.5965c571-36c0-485a-92dd-27ef4f35cade'
    );
    private $db_conn;
    private $userID;

    /**
     * RequestHandler constructor.
     * @param $id string the calling skill's application id
     * @param $accessToken string the accessToken saved in Alexa
     * @param null $db_conn
     */
    public function __construct($id, $accessToken = null, $db_conn = null){
        $this->db_conn = $db_conn;
        if (!$this->verifyApplicationId($id))
            die (ErrorHandler::buildErrorNoResponse('This skill is not supported by the Lincus Endpoint. Erroneous value: ' . $id));

        if (ACCOUNT_LINKING) {
            if (!is_null($accessToken)) {
                if (!$this->verifyUser($accessToken))
                    die (ErrorHandler::buildLoginError("To connect your Lincus Account please click on the link in your latest alexa card"));
            } else {
                die (ErrorHandler::buildError("To connect your Lincus Account please click on the link in your latest alexa card"));
            }
        }
    }

    /**
     * <h2>verifyApplicationId</h2>
     * <br />
     * Verifies the applicationId is a supported application by this endpoint.
     * @param $applicationId string the application id to verify
     * @return bool is supported?
     */
    private function verifyApplicationId($applicationId){
        foreach ($this->supported_application_ids as $value){
            if ($applicationId == $value){
                return true;
            }
        }
        return false;
    }

    /**
     * <h2>verifyUser</h2>
     * <br />
     * Verify the access token exists in the Lincus Database
     * @param $accessToken String access token sent from Alexa
     * @return bool if access token exists
     */
    private function verifyUser($accessToken){
        //check the value of the accessToken in the Database
        try {
            //$this->db_conn->beginTransaction();
            $query = $this->db_conn->prepare("SELECT `user_id`, `expires` FROM oauth_access_tokens WHERE access_token =:accessToken");
            $query->bindValue(":accessToken", $accessToken);
            $query->execute();

            $results = $query->fetch(PDO::FETCH_ASSOC);

            $authenticated = array(FALSE, FALSE);

            //TODO check that the user exists in the Lincus DB.
            if ($results != false) {
                foreach ($results as $key => $value) {
                    //check that the accessToken has not expired yet.
                    if ($key == "expires") {
                        $today = strtotime("now");
                        $expires = strtotime($value);
                        if ($expires >= $today) {
                            $authenticated[0] = TRUE;
                        } else {
                            ErrorHandler::buildErrorNoResponse("Alexa did not renew Access Token. Access token: " . $accessToken);
                        }
                    }

                    //Check the user still exists and has not been deleted
                    if ($key == "user_id"){
                        $query2 = $this->db_conn->prepare("SELECT `user_id` FROM `user_ids` WHERE `user_id` =:id");
                        $query2->bindValue(":id", (int)$value);
                        $query2->execute();

                        $users = $query2->fetch(PDO::FETCH_ASSOC);
                        if ($users != false) {
                            foreach ($users as $key2 => $value2) {
                                if ($value2 == $value) {
                                    $this->userID = $value2;
                                    $authenticated[1] = TRUE;
                                }
                            }
                        }
                    }
                }
            }
            if (($authenticated[0] === TRUE) AND ($authenticated[1] === TRUE)){
                return true;
            }
            //Not renewed or alexa not linked
            return false;

        } catch (PDOException $e){
            die (ErrorHandler::buildErrorNoResponse("An error occurred checking access token: " . $e->getMessage()));
        }
    }

    public function getUserId(){
        return $this->userID;
    }
}