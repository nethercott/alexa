<?php
/**
 * <h1>ResponseHandler</h1>
 * <br />
 * Create any responses to send to the Alexa enabled device in this class. You are able to create the following responses:
 * <ul>
 *      <li>Ask Response - Does not end session</li>
 *      <li>Tell Response - Does end session</li>
 *      <li>Link Response - Starts account Linking process</li>
 * </ul>
 * @author Matthew Nethercott
 * @version 1.0.0
 * @since 8th June 2018
 */

class ResponseHandler {

    private $responseMessage;

    public function __construct(){
        $this->responseMessage = new \ArrayObject();
    }

    /**
     * <h2>newTellResponse</h2>
     * <br />
     * Create a new response to send to the Alexa enabled device (Will end the session, no response)
     * @param $response String what to tell the user
     */
    public function newTellResponse($response){
        $this->responseMessage["outputSpeech"] = $this->generateOutputSpeech($response);
        $this->responseMessage["reprompt"] = $this->generateRepromt($response);
        $this->responseMessage["shouldEndSession"] = true;
    }

    /**
     * <h2>newTellResponse</h2>
     * <br />
     * Create a new response to send to the Alexa enabled device (Will not end session (Expect response))
     * @param $response String what to tell the user
     */
    public function newAskResponse($response){
        $this->responseMessage["outputSpeech"] = $this->generateOutputSpeech($response);
        $this->responseMessage["reprompt"] = $this->generateRepromt($response);
        $this->responseMessage["shouldEndSession"] = false;
    }

    /**
     * <h2>newLinkResponse</h2>
     * <br />
     * Used when the user needs to link their account to the Skill
     * @param $response String what to notify the user
     */
    public function newLinkResponse($response, $cardTitle){
        $this->responseMessage["outputSpeech"] = $this->generateOutputSpeech($response);
        $this->responseMessage["reprompt"] = $this->generateRepromt($response);

        $card = new CardResponse();
        $card->createLinked($cardTitle);

        $this->responseMessage["card"] = $card->getCard();

        $this->responseMessage["shouldEndSession"] = true;
    }

    /**
     * <h2>getResponse</h2>
     * <br />
     * Return the message back to the calling line. Should be ready to send back to the Alexa Device
     * @return ArrayObject
     */
    public function getResponse(){
        return $this->responseMessage;
    }

    /**
     * <h2>generateOutputSpeech</h2>
     * <br />
     * Generates output speech to say to the user
     * @param $outputSpeech
     * @return array
     */
    private function generateOutputSpeech($outputSpeech){
        return array(
            "type" => "PlainText",
            "text" => $outputSpeech
        );
    }

    /**
     * <h2>generateReprompt</h2>
     * <br />
     * Generates the reprompt text to reprompt the user with Alexa.
     * @param $reprompt String saying to reprompt the user with
     * @return array
     */
    private function generateRepromt($reprompt){
        return array(
            "outputSpeech" => $this->generateOutputSpeech($reprompt)
        );
    }
}