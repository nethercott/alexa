<?php
/**
 * <h1>CardResponse</h1>
 * <br />
 * Create card responses to send back to the Alexa Enabled device.
 * Can create any of the following cards
 * <ul>
 *     <li>Simple</li>
 *     <li>Standard</li>
 *     <li>Linked</li>
 * </ul>
 * @author Matthew Nethercott
 * @version 1.0.0
 * @since 8th June 2018
 */

class CardResponse {

    private $card;

    //Card Types
    const SIMPLE = "Simple";
    const LINK = "LinkAccount";
    const STANDARD = "Standard";
    const PERMISSIONS = "AskForPermissionsConsent";

    public function __construct(){
        $this->card = new \ArrayObject();
    }

    /**
     * <h2>createSimple</h2>
     * <br />
     * Create a simple card to return to the Alexa enabled device
     * @param $title String
     * @param $text String
     */
    public function createSimple($title, $text){
        $this->card["type"] = self::SIMPLE;
        $this->card["title"] = $title;
        $this->card["content"] = $text;
    }

    /**
     * <h2>createLinked</h2>
     * <br />
     * Create a linked card to return to the Alexa enabled device. Will prompt the user to link their account
     * @param $title String
     * @param $text String
     */
    public function createLinked($title){
        $this->card["type"] = self::LINK;
        $this->card["title"] = $title;
    }

    /**
     * <h2>createStandard</h2>
     * <br />
     * Create a card with an image to return to the alexa enabled device
     * @param $title String
     * @param $text String
     * @param $image CardImage
     */
    public function createStandard($title, $text, $image){
        $this->card["type"] = self::STANDARD;
        $this->card["title"] = $title;
        $this->card["content"] = $text;
        //$this->card["image"] = $image;
    }

    public function createPermissionRequestCard($permissionsToGrant){
        $this->card["type"] = self::PERMISSIONS;
        $this->card["permissions"] = array($permissionsToGrant);
    }

    public function getCard(){
        return $this->card;
    }
}