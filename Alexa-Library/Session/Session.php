<?php
/**
 * <h1>Session</h1>
 * <br />
 * Handles all the data for sessions for the amazon alexa device.
 * @author Matthew Nethercott
 * @version 1.0
 * @since 11 June 2018
 */

class Session {
    private $session;

    public function __construct($currentSession = null){
        if (is_null($currentSession))
            $this->session = new \ArrayObject();
        else
            $this->session = $currentSession;
    }

    /**
     * <h2>addToSession</h2>
     * <br />
     * Add data to Alexa session
     * @param $name String name of session variable
     * @param $data String value of the session variable
     */
    public function addToSession($name, $data){
        $this->session[$name] = $data;
    }

    public function getSession(){
        return $this->session;
    }
}