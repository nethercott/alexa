<?php
/**
 * <h1>IntentHandler</h1>
 * <br />
 * Handle the intent data sent by the alexa device. Any data to be pulled from the JSON intent object handle here
 * @author Matthew Nethercott
 * @version 1.0.0
 * @since 8th June 2018
 */

class IntentHandler{

    private $intentSent;
    private $permissionFail;
    private $address;

    public function __construct($intent){
        $this->intentSent = $intent;
        $this->permissionFail = false;
    }

    /**
     * <h2>getAddress</h2>
     * <br />
     * get the address of the alexa device
     * @param $apiAccessToken String accessToken sent from the alexa device
     * @param $apiEndpoint String endpoint to get the address from
     * @param $deviceId String id value of the device (sent from alexa)
     */
    public function getAddress($apiAccessToken, $apiEndpoint, $deviceId){
        //END POINT = /v1/devices/*deviceId*/settings/address

        $concatURI = "/v1/devices/" . $deviceId . "/settings/address";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $apiEndpoint . $concatURI);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("accept: application/json", "Authorization: Bearer " . $apiAccessToken));

        $output = json_decode(curl_exec($ch), true);
        ErrorHandler::buildErrorNoResponse("Output is: ". json_encode($output));

        curl_close($ch);

        if (!is_null($output)) {
            if (array_key_exists("type", $output)) {
                //PERMISSION CARD HERE
                $this->permissionFail = true;
            } else {
                $this->address = $output;
            }
        }
    }

    /**
     * <h2>getIntenName</h2>
     * <br />
     * Return the intent name from the alexa json
     * @return mixed
     */
    public function getIntentName(){
        return $this->intentSent["name"];
    }

    /**
     * <h2>getIntentSlotValue</h2>
     * <br />
     * Get value of any slots in intent
     * @param $slot String slot name to access
     * @return mixed
     */
    public function getIntentSlotValue($slot){
        return $this->intentSent["slots"][$slot]["value"];
    }

    public function getPermissionFail(){
        return $this->permissionFail;
    }

    public function getAddressSaved(){
        return $this->address;
    }
}