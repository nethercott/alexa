<?php


require_once('Alexa-Library/RequestHandler.php');
require_once('Alexa-Library/ErrorHandler.php');
require_once("Alexa-Library/Response/ResponseHandler.php");
require_once("Alexa-Library/Response/CardResponse.php");
require_once("Alexa-Library/Session/Session.php");
require_once("Alexa-Library/IntentHandler.php");
require_once("AlexaClass.php");

//GLOBAL CONSTANTS
define("CARD_TITLE", "Lincus Alerts");
//End of constants


/*
 * DELETE THIS SECTION ON MOVE TO LINCUS
 */
$pdo_dsn 	= "mysql:host=nethercott.biz.mysql;dbname=nethercott_biz;charset=utf8";
$pdo_user 	= "nethercott_biz";
$pdo_pass 	= "fd@s\"43gfd";
try {
    //CONNECT TO THE DATABASE
    $db_conn = new PDO($pdo_dsn, $pdo_user, $pdo_pass);
    $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db_conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $db_conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch(PDOException $ex){
    die(ErrorHandler::buildErrorNoResponse("Cannot connect to the database"));
}
/*
 * END OF DELETE
 */

$payload = json_decode(file_get_contents("php://input"), true);

//INTIALISE DATA SENT FROM ALEXA
//get session data and verification data
$session = $payload["session"];
$applicationId = $session["application"]["applicationId"];
$user = $session["user"];

//Get the request and the intent sent from Alexa
$request = $payload["request"];
$accessToken = $user["accessToken"];

//setup the handler of the this data and verify the request
//if unverified will die in this class
$handler = new RequestHandler($applicationId, $accessToken, $db_conn);

//Setup the intent
if ($request["type"] == "IntentRequest")    $intent = $request["intent"];
$intentName = $intent["name"];
$intentHandler = new IntentHandler($intent);

$intentHandler->getAddress($payload["context"]["System"]["apiAccessToken"], $payload["context"]["System"]["apiEndpoint"]
    , $payload["context"]["System"]["device"]["deviceId"]);

//get the current session or start a new one
if (array_key_exists("attributes", $session)) $currentSession = new Session($session["attributes"]);
else $currentSession = new Session();

//END OF INITIALISATION

$response = new ResponseHandler();
$card = null;
$conditionSlotValue = null;
$default = false;
//Process the data
switch($intentName){
    case "ConnectIntent":
        $response->newTellResponse("To connect your Lincus Account please click on the link in your latest alexa card");
        $card = new CardResponse();
        $card->createLinked(CARD_TITLE);
        break;
    case "TriggerIntent":
        $response->newAskResponse("Which alert would you like to trigger?");
        $currentSession->addToSession("Stage", 1);
        $currentSession->addToSession("Response", "triggered");
        break;
    case "ResolveIntent":
        $response->newAskResponse("Which alert would you like to resolve?");
        $currentSession->addToSession("Stage", 1);
        $currentSession->addToSession("Response", "resolved");
        break;
    case "ConditionIntent":
        //Check if they are able to add a condition yet
        if ($currentSession->getSession()["Stage"] == 1){
            $conditionSlotValue = $intentHandler->getIntentSlotValue("Condition");
            $response->newTellResponse("The alert for your " . $conditionSlotValue
                . " has been " . $currentSession->getSession()["Response"]);
            $card = new CardResponse();
            $card->createSimple(CARD_TITLE, "Your alert for " . $conditionSlotValue . " has been "
                . $currentSession->getSession()["Response"] . ".");
        } else {
            $response->newTellResponse("Please say alexa, start lincus for how to use this skill");
        }
        break;
    case "TriggerWithIntent":
        $conditionSlotValue = $intentHandler->getIntentSlotValue("Condition");
        $response->newTellResponse("The alert for your " . $conditionSlotValue . " triggered.");
        $card = new CardResponse();
        $card->createSimple(CARD_TITLE, "Your alert for " . $conditionSlotValue . " has been triggered.");
        break;
    case "ResolveWithIntent":
        $conditionSlotValue = $intentHandler->getIntentSlotValue("Condition");
        $response->newTellResponse("The alert for your " . $conditionSlotValue . " triggered.");
        $card = new CardResponse();
        $card->createSimple(CARD_TITLE, "Your alert for " . $conditionSlotValue . " has been resolved.");
        break;
    default:
        $default = true;
        $response->newTellResponse("This command is not supported by this skill. Please try a different command");
        $card = new CardResponse();
        $card->createSimple(CARD_TITLE, "This command is not supported by this skill. Please try a different command");
        break;
}
//End of processing

//only save to the db if the default command was not returned
if (!$default){
    $alexa = new AlexaClass($db_conn, $handler->getUserId());
    $alexa->insertDetail($intentName, $conditionSlotValue, $intentHandler->getAddressSaved());
}

//CHECK IF PERMISSIONS FOR ADDRESS FAILED
if ($intentHandler->getPermissionFail()){
    $card = new CardResponse();
    $card->createPermissionRequestCard("read::alexa:device:all:address");
}


//Build Response here
$responseToSend = new \ArrayObject();
$responseToSend["version"] = "1.0";
$responseToSend["sessionAttributes"] = $currentSession->getSession();
$responseToSend["response"] = $response->getResponse();
if (!is_null($card))    $responseToSend["response"]["card"] = $card->getCard();
//End of build


//Return the data back to the Alexa Enabled device
die (json_encode($responseToSend));